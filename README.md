# README #

## GITLAB-RUNNER ##

### Clonar repositorio ###
* git clone https://setting-ejemplo@bitbucket.org/setting-ejemplo/gitlab-runner.git

### Crear imagen
* docker build -t settingejemplo/gitlab-runner .

### Crear contenedor ###
* docker run \
  --detach \
  --name gitlab-runner-container \
  --restart always \
  --network setting \
  --ip 172.13.1.11 \
  --volume /srv/gitlab-runner/config:/etc/gitlab-runner \
  --volume /var/run/docker.sock:/var/run/docker.sock \
  settingejemplo/gitlab-runner
